#!/bin/bash

python3 -m venv /opt/tunneldigger_venv
. /opt/tunneldigger_venv/bin/activate

cd /opt/tunneldigger/broker
python setup.py install
